import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ChatBackground from '../../components/ChatBackground/ChatBackground'
import ChatLayout from '../../components/ChatLayout/ChatLayout'
import ChatMenu from '../../components/ChatMenu/ChatMenu'
import ChatMainWindow from '../../components/ChatMainWindow/ChatMainWindow'
import ChatSearch from '../../components/ChatSearch/ChatSearch'
import ChatList from '../../components/ChatList/Chatlist'
import ChatMessage from '../../components/ChatMessage/ChatMessage'
import {
  chats,
  searchChat,
  statusChats,
} from '../../redux/reducers/ChatReducer/selectors'
import {
  getChatsCreator,
  asyncSearchChatsCreator,
} from '../../redux/reducers/ChatReducer/ChatReducer'
import { email } from '../../redux/reducers/FormReducer/selectors'
import {
  replaceSymbols,
  getTimeHuman,
  limitationStringEllipsis,
} from '../../services/auxiliary'
import { Spinner } from 'reactstrap'
import debounce from 'lodash.debounce'
import s from './ClosedMessages.module.css'

const ClosedMessages = () => {
  const [search, setSearch] = useState('')
  const [typeSearch, setTypeSearch] = useState('nickname')

  const dispatch = useDispatch()
  const closedChats = useSelector(chats)
  const userEmail = useSelector(email)
  const status = useSelector(statusChats)
  const searchClosedChats = useSelector(searchChat)

  const searchChangeHandler = debounce((text) => {
    setSearch(text)
  }, 1000)

  const typeChangeHandler = debounce((text) => {
    setTypeSearch(text)
  }, 1000)

  useEffect(() => {
    dispatch(
      getChatsCreator({
        ref: replaceSymbols(userEmail),
        status: 'closed',
      })
    )
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    dispatch(
      asyncSearchChatsCreator({
        ref: replaceSymbols(userEmail),
        typeSearch: typeSearch,
        search: search,
        status: 'closed',
      })
    )
    // eslint-disable-next-line
  }, [search, typeSearch])

  const renderChats = () => {
    if (!search) {
      return closedChats.map((item) => (
        <ChatMessage
          key={item.key}
          clientName={item?.content?.client}
          dateStart={getTimeHuman(item?.content?.timestamp)}
          messageText={limitationStringEllipsis(
            item?.content?.messages[0]?.content,
            250
          )}
          textSavedButton="Сохранить диалог"
          textLink="Удалить диалог"
          iconClass="far fa-save"
          dataClick="delete"
          typeClickLink="save"
          _id={item?.key}
        />
      ))
    }
    if (search && closedChats.length > 0) {
      return (
        searchClosedChats &&
        searchClosedChats.map((item) => (
          <ChatMessage
            key={item.key}
            clientName={item?.content?.client}
            dateStart={getTimeHuman(item?.content?.timestamp)}
            messageText={limitationStringEllipsis(
              item?.content?.messages[0]?.content,
              250
            )}
            textSavedButton="Сохранить диалог"
            textLink="Удалить диалог"
            iconClass="far fa-save"
            dataClick="delete"
            typeClickLink="save"
            _id={item?.key}
          />
        ))
      )
    }
  }

  return (
    <div className={s.closed}>
      <ChatBackground>
        <ChatLayout>
          <ChatMenu />
          <ChatMainWindow className={s.messagebox}>
            <ChatSearch
              searchChangeHandler={(e) => {
                searchChangeHandler(e.target.value)
              }}
              typeChangeHandler={(e) => typeChangeHandler(e.target.value)}
            />
            <ChatList>
              {status === 'success' && closedChats?.length > 0 ? (
                renderChats()
              ) : closedChats?.length === 0 || status === 'failed' ? (
                <div>Завершенных чатов нет!</div>
              ) : (
                <Spinner />
              )}
            </ChatList>
          </ChatMainWindow>
        </ChatLayout>
      </ChatBackground>
    </div>
  )
}

export default ClosedMessages

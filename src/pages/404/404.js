import s from './Page404.module.css'
const Page404 = () => {
  return (
    <div className={s.page404}>
      <h2 className={s.title}>Ошибка 404!</h2>
      <p className={s.subtitle}>Страница не найдена</p>
    </div>
  )
}
export default Page404

import { ReloadForm } from '../../components/Forms'
import s from './ReloadPass.module.css'
const ReloadPass = () => {
  return (
    <div className={s.reload}>
      <ReloadForm />
    </div>
  )
}

export default ReloadPass

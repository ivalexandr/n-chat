import { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router'
import ChatBackground from '../../components/ChatBackground/ChatBackground'
import ChatLayout from '../../components/ChatLayout/ChatLayout'
import ChatMainWindow from '../../components/ChatMainWindow/ChatMainWindow'
import ChatMenu from '../../components/ChatMenu/ChatMenu'
import ChatMessageItem from '../../components/ChatMessageItem/ChatMessageItem'
import { useFormik } from 'formik'
import { Input, Label } from 'reactstrap'
import FormButton from '../../components/FormButton/FormButton'
import { email } from '../../redux/reducers/FormReducer/selectors'
import {
  messages,
  targetMessage,
} from '../../redux/reducers/ChatReducer/selectors'
import {
  closedGetNewMessagesCreator,
  newMessageCreator,
  setActiveCreator,
} from '../../redux/reducers/ChatReducer/ChatReducer'
import { getTimeHuman, replaceSymbols } from '../../services/auxiliary'
import { Spinner } from 'reactstrap'
import s from './Dialog.module.css'

const validate = (values) => {
  let errors = {}
  if (!values.message) errors.message = 'Сообщение не может быть пустым'
  if (values.answers) errors = {}
  return errors
}

const Dialog = () => {
  const dispatch = useDispatch()
  const userEmail = useSelector(email)
  const userMessages = useSelector(messages)
  const target = useSelector(targetMessage)
  const { id } = useParams()
  const list = useRef()
  const formik = useFormik({
    initialValues: {
      message: '',
      answers: '',
    },
    validate,
    onSubmit: (values, formikBag) => {
      dispatch(
        setActiveCreator({
          ref: `${replaceSymbols(userEmail)}/${target[0]}/messages`,
          data: {
            client: false,
            content: values.message,
          },
        })
      )
      formikBag.resetForm()
    },
    validateOnBlur: false,
    validateOnChange: false,
    validateOnMount: false,
  })

  useEffect(() => {
    dispatch(
      newMessageCreator({
        ref: replaceSymbols(userEmail),
        type: 'messages',
        id,
      })
    )
    return () => {
      dispatch(closedGetNewMessagesCreator())
    }
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    list.current.scrollTop = list.current.scrollHeight
  }, [userMessages])

  const renderMessages = () => {
    if (userMessages) {
      return userMessages?.map((item) => (
        <ChatMessageItem
          key={item[0]}
          text={item[1]?.content}
          client={item[1]?.client}
          time={getTimeHuman(item[1]?.timestamp)}
        />
      ))
    } else {
      return <Spinner type="primary" />
    }
  }
  return (
    <div className={s.dialog}>
      <ChatBackground>
        <ChatLayout>
          <ChatMenu />
          <ChatMainWindow className={s.messagebox}>
            <h2 className={s.client}>{id}</h2>
            <ul className={s.list} ref={list}>
              {renderMessages()}
            </ul>
            <form action="#" onSubmit={formik.handleSubmit} className={s.form}>
              <div className={s.input}>
                <Input
                  type="text"
                  onChange={formik.handleChange}
                  value={formik.values.message}
                  placeholder="Введите сообщение"
                  name="message"
                />
                <FormButton type="submit">Отправить</FormButton>
              </div>
              {<div className={s.error}>{formik.errors.message}</div>}
              <div className={s.dropdown}>
                <Label for="answers"> Выберите готовые ответ </Label>
                <Input
                  type="select"
                  onChange={formik.handleChange}
                  name="answers"
                  value={formik.values.answers}
                  id="answers"
                >
                  <option>ответ 1</option>
                  <option>ответ 2</option>
                  <option>ответ 3</option>
                  <option>ответ 4</option>
                </Input>
              </div>
            </form>
          </ChatMainWindow>
        </ChatLayout>
      </ChatBackground>
    </div>
  )
}

export default Dialog

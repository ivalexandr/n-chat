import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  asyncSearchChatsCreator,
  getChatsCreator,
} from '../../redux/reducers/ChatReducer/ChatReducer'
import ChatBackground from '../../components/ChatBackground/ChatBackground'
import ChatLayout from '../../components/ChatLayout/ChatLayout'
import Chatlist from '../../components/ChatList/Chatlist'
import ChatMainWindow from '../../components/ChatMainWindow/ChatMainWindow'
import ChatMenu from '../../components/ChatMenu/ChatMenu'
import ChatMessage from '../../components/ChatMessage/ChatMessage'
import ChatSearch from '../../components/ChatSearch/ChatSearch'
import { email } from '../../redux/reducers/FormReducer/selectors'
import {
  getTimeHuman,
  limitationStringEllipsis,
  replaceSymbols,
} from '../../services/auxiliary'
import {
  chats,
  searchChat,
  statusChats,
} from '../../redux/reducers/ChatReducer/selectors'
import { Spinner } from 'reactstrap'
import debounce from 'lodash.debounce'
import s from './ActiveMessages.module.css'

const ActiveMessages = () => {
  const [search, setSearch] = useState('')
  const [typeSearch, setTypeSearch] = useState('nickname')

  const dispatch = useDispatch()
  const userEmail = useSelector(email)
  const status = useSelector(statusChats)
  const activeChats = useSelector(chats)
  const searchActiveChats = useSelector(searchChat)

  const searchChangeHandler = debounce((text) => {
    setSearch(text)
  }, 1000)

  const typeChangeHandler = debounce((text) => {
    setTypeSearch(text)
  }, 1000)

  useEffect(() => {
    dispatch(
      getChatsCreator({ ref: replaceSymbols(userEmail), status: 'active' })
    )
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    dispatch(
      asyncSearchChatsCreator({
        ref: replaceSymbols(userEmail),
        typeSearch: typeSearch,
        search: search,
        status: 'active',
      })
    )
    // eslint-disable-next-line
  }, [search, typeSearch])

  const renderChats = () => {
    if (!search) {
      return activeChats.map((item) => (
        <ChatMessage
          key={item.key}
          clientName={item?.content?.client}
          dateStart={getTimeHuman(item?.content?.timestamp)}
          messageText={limitationStringEllipsis(
            item?.content?.messages[0]?.content,
            250
          )}
          textSavedButton="Завершить диалог"
          textLink="Продолжить диалог"
          iconClass="far fa-times-circle"
          dataClick="continue"
          typeClickLink="close"
          _id={item?.key}
        />
      ))
    }
    if (search && activeChats.length > 0) {
      return (
        searchActiveChats &&
        searchActiveChats.map((item) => (
          <ChatMessage
            key={item.key}
            clientName={item?.content?.client}
            dateStart={getTimeHuman(item?.content?.timestamp)}
            messageText={limitationStringEllipsis(
              item?.content?.messages[0]?.content,
              250
            )}
            textSavedButton="Завершить диалог"
            textLink="Продолжить диалог"
            iconClass="far fa-times-circle"
            dataClick="continue"
            typeClickLink="close"
            _id={item?.key}
          />
        ))
      )
    }
  }

  return (
    <div className={s.activemessages}>
      <ChatBackground>
        <ChatLayout>
          <ChatMenu />
          <ChatMainWindow className={s.messagebox}>
            <ChatSearch
              searchChangeHandler={(e) => {
                searchChangeHandler(e.target.value)
              }}
              typeChangeHandler={(e) => typeChangeHandler(e.target.value)}
            />
            <Chatlist>
              {status === 'success' && activeChats?.length > 0 ? (
                renderChats()
              ) : status === 'failed' || activeChats?.length === 0 ? (
                <div>Активных чатов нет!</div>
              ) : (
                <Spinner />
              )}
            </Chatlist>
          </ChatMainWindow>
        </ChatLayout>
      </ChatBackground>
    </div>
  )
}

export default ActiveMessages

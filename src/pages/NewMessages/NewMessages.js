import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ChatBackground from '../../components/ChatBackground/ChatBackground'
import ChatLayout from '../../components/ChatLayout/ChatLayout'
import Chatlist from '../../components/ChatList/Chatlist'
import ChatMainWindow from '../../components/ChatMainWindow/ChatMainWindow'
import ChatMenu from '../../components/ChatMenu/ChatMenu'
import ChatMessage from '../../components/ChatMessage/ChatMessage'
import ChatSearch from '../../components/ChatSearch/ChatSearch'
import {
  closedGetNewMessagesCreator,
  newMessageCreator,
  syncSearchChatCreator,
} from '../../redux/reducers/ChatReducer/ChatReducer'
import {
  newMessages,
  searchChat,
} from '../../redux/reducers/ChatReducer/selectors'
import {
  getTimeHuman,
  limitationStringEllipsis,
  syncSearchChats,
} from '../../services/auxiliary'
import s from './NewMessages.module.css'

const NewMessages = () => {
  const [search, setSearch] = useState('')
  const [typeSearch, setTypeSearch] = useState('nickname')

  const dispatch = useDispatch()
  const messages = useSelector(newMessages)
  const searchMessages = useSelector(searchChat)

  const searchChangeHandler = (text) => {
    setSearch(text)
  }
  const typeChangeHandler = (text) => {
    setTypeSearch(text)
  }

  useEffect(() => {
    dispatch(
      newMessageCreator({
        ref: 'new',
        type: 'chats',
      })
    )
    return () => {
      dispatch(closedGetNewMessagesCreator('new'))
    }
    // eslint-disable-next-line
  }, [])
  useEffect(() => {
    if (search)
      setTimeout(() => {
        dispatch(
          syncSearchChatCreator(syncSearchChats(messages, typeSearch, search))
        )
      }, 0)
    // eslint-disable-next-line
  }, [search])
  const renderChats = () => {
    if (!search) {
      return messages ? (
        messages.map((item) => (
          <ChatMessage
            key={item?.key}
            messageText={limitationStringEllipsis(
              item?.content?.messages?.[0].content,
              250
            )}
            clientName={item?.content?.client}
            textLink="Начать диалог"
            dateStart={getTimeHuman(item?.content?.messages?.[0].timestamp)}
            dataClick="open"
          />
        ))
      ) : (
        <div>Новых сообщений нет</div>
      )
    }
    return (
      searchMessages &&
      searchMessages.map((item) => (
        <ChatMessage
          key={item?.key}
          messageText={limitationStringEllipsis(
            item?.content?.messages?.[0].content,
            250
          )}
          clientName={item?.content?.client}
          textLink="Начать диалог"
          dateStart={getTimeHuman(item?.content?.messages?.[0].timestamp)}
          dataClick="open"
        />
      ))
    )
  }
  return (
    <div className={s.new}>
      <ChatBackground>
        <ChatLayout>
          <ChatMenu />
          <ChatMainWindow
            className={s.messagebox}
            queue={messages ? messages?.length.toString() : '0'}
          >
            <ChatSearch
              searchChangeHandler={(e) => searchChangeHandler(e.target.value)}
              typeChangeHandler={(e) => typeChangeHandler(e.target.value)}
            />
            <Chatlist>{renderChats()}</Chatlist>
          </ChatMainWindow>
        </ChatLayout>
      </ChatBackground>
    </div>
  )
}

export default NewMessages

import { ForgottenForm } from '../../components/Forms'
import s from './Forgotten.module.css'
const Forgotten = () => {
  return (
    <div className={s.forgotten}>
      <ForgottenForm />
    </div>
  )
}

export default Forgotten

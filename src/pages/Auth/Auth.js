import { useEffect } from 'react'
import { AuthForm } from '../../components/Forms'
import { useSelector } from 'react-redux'
import { user } from '../../redux/reducers/FormReducer/selectors'
import { useHistory } from 'react-router'
import s from './Auth.module.css'

const Auth = () => {
  const token = useSelector(user)
  const history = useHistory()
  useEffect(() => {
    if (token) history.push('/')
    // eslint-disable-next-line
  }, [token])

  return (
    <div className={s.auth}>
      <AuthForm />
    </div>
  )
}
export default Auth

import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import ChatBackground from '../../components/ChatBackground/ChatBackground'
import ChatLayout from '../../components/ChatLayout/ChatLayout'
import Chatlist from '../../components/ChatList/Chatlist'
import ChatMainWindow from '../../components/ChatMainWindow/ChatMainWindow'
import ChatMenu from '../../components/ChatMenu/ChatMenu'
import ChatSearch from '../../components/ChatSearch/ChatSearch'
import ChatMessage from '../../components/ChatMessage/ChatMessage'
import {
  getChatsCreator,
  asyncSearchChatsCreator,
} from '../../redux/reducers/ChatReducer/ChatReducer'
import { email } from '../../redux/reducers/FormReducer/selectors'
import {
  replaceSymbols,
  getTimeHuman,
  limitationStringEllipsis,
} from '../../services/auxiliary'
import {
  chats,
  searchChat,
  statusChats,
} from '../../redux/reducers/ChatReducer/selectors'
import { Spinner } from 'reactstrap'
import debounce from 'lodash.debounce'
import s from './SaveMessages.module.css'

const SaveMessages = () => {
  const [search, setSearch] = useState('')
  const [typeSearch, setTypeSearch] = useState('nickname')

  const dispatch = useDispatch()
  const userEmail = useSelector(email)
  const savedChats = useSelector(chats)
  const status = useSelector(statusChats)
  const searchSavedChats = useSelector(searchChat)

  const searchChangeHandler = debounce((text) => {
    setSearch(text)
  }, 1000)

  const typeChangeHandler = debounce((text) => {
    setTypeSearch(text)
  }, 1000)

  useEffect(() => {
    dispatch(
      getChatsCreator({
        ref: replaceSymbols(userEmail),
        status: 'saved',
      })
    )
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    dispatch(
      asyncSearchChatsCreator({
        ref: replaceSymbols(userEmail),
        typeSearch: typeSearch,
        search: search,
        status: 'saved',
      })
    )
    // eslint-disable-next-line
  }, [search, typeSearch])

  const renderChats = () => {
    if (!search) {
      return savedChats.map((item) => (
        <ChatMessage
          key={item.key}
          clientName={item?.content?.client}
          dateStart={getTimeHuman(item?.content?.timestamp)}
          messageText={limitationStringEllipsis(
            item?.content?.messages[0]?.content,
            250
          )}
          textSavedButton="Удалить диалог"
          iconClass="far fa-trash-alt"
          typeClickLink="delete"
          _id={item?.key}
        />
      ))
    }
    if (search && savedChats.length > 0) {
      return (
        searchSavedChats &&
        searchSavedChats.map((item) => (
          <ChatMessage
            key={item.key}
            clientName={item?.content?.client}
            dateStart={getTimeHuman(item?.content?.timestamp)}
            messageText={limitationStringEllipsis(
              item?.content?.messages[0]?.content,
              250
            )}
            textSavedButton="Удалить диалог"
            iconClass="far fa-trash-alt"
            typeClickLink="delete"
            _id={item?.key}
          />
        ))
      )
    }
  }

  return (
    <div className={s.saved}>
      <ChatBackground>
        <ChatLayout>
          <ChatMenu />
          <ChatMainWindow className={s.messagebox}>
            <ChatSearch
              searchChangeHandler={(e) => {
                searchChangeHandler(e.target.value)
              }}
              typeChangeHandler={(e) => typeChangeHandler(e.target.value)}
            />
            <Chatlist>
              {status === 'success' && savedChats?.length > 0 ? (
                renderChats()
              ) : savedChats?.length === 0 || status === 'failed' ? (
                <div>Сохраненных чатов нет!</div>
              ) : (
                <Spinner />
              )}
            </Chatlist>
          </ChatMainWindow>
        </ChatLayout>
      </ChatBackground>
    </div>
  )
}
export default SaveMessages

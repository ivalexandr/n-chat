import { useEffect } from 'react'
import { RegForm } from '../../components/Forms'
import { useSelector } from 'react-redux'
import { user } from '../../redux/reducers/FormReducer/selectors'
import { useHistory } from 'react-router'
import s from './Registration.module.css'

const Registration = () => {
  const token = useSelector(user)
  const history = useHistory()

  useEffect(() => {
    if (token) history.push('/')
    // eslint-disable-next-line
  }, [token])

  return (
    <div className={s.reg}>
      <RegForm />
    </div>
  )
}

export default Registration

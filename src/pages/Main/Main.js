import ChatBackground from '../../components/ChatBackground/ChatBackground'
import ChatLayout from '../../components/ChatLayout/ChatLayout'
import ChatMenu from '../../components/ChatMenu/ChatMenu'
import ChatMainWindow from '../../components/ChatMainWindow/ChatMainWindow'
import s from './Main.module.css'

const Main = () => {
  return (
    <div className={s.main}>
      <ChatBackground>
        <ChatLayout>
          <ChatMenu />
          <ChatMainWindow>
            <div className={s.placeholder}>Выберите раздел...</div>
          </ChatMainWindow>
        </ChatLayout>
      </ChatBackground>
    </div>
  )
}

export default Main

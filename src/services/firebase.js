import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const firebaseConfig = {
  apiKey: 'AIzaSyBDqSz31aFrjWAOJ5GdFlth1vwObeVpTRM',
  authDomain: 'n-chat-2a591.firebaseapp.com',
  databaseURL:
    'https://n-chat-2a591-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'n-chat-2a591',
  storageBucket: 'n-chat-2a591.appspot.com',
  messagingSenderId: '571140906865',
  appId: '1:571140906865:web:6d43900a57ab7c9f54e0d0',
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

class FirebaseApi {
  async authUser(data) {
    try {
      const res = await firebase
        .auth()
        .signInWithEmailAndPassword(data.email, data.password)
      return {
        email: res.user.email,
        token: res.user.getIdToken(),
        refreshToken: res.user.refreshToken,
      }
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async regUser(data) {
    try {
      const res = await firebase
        .auth()
        .createUserWithEmailAndPassword(data.email, data.password)
      return {
        email: res.user.email,
        token: res.user.getIdToken(),
        refreshToken: res.user.refreshToken,
      }
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async resetPassword(email) {
    try {
      await firebase.auth().sendPasswordResetEmail(email)
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async updatePassword(data) {
    try {
      await firebase.auth().confirmPasswordReset(data.token, data.password)
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async authWithGoogle() {
    try {
      const provider = new firebase.auth.GoogleAuthProvider()
      const res = await firebase.auth().signInWithPopup(provider)
      const credential = res.credential
      return {
        token: credential.accessToken,
        user: res.user,
        refreshToken: res.user.refreshToken,
        email: res.user.email,
      }
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async authWithGithub() {
    try {
      const provider = new firebase.auth.GithubAuthProvider()
      const res = await firebase.auth().signInWithPopup(provider)
      const credential = res.credential
      return {
        token: credential.accessToken,
        user: res.user,
        refreshToken: res.user.refreshToken,
        email: res.user.email,
      }
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async logoutUser() {
    try {
      await firebase.auth().signOut()
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async refreshUser(token) {
    try {
      const api = `https://securetoken.googleapis.com/v1/token?key=AIzaSyBDqSz31aFrjWAOJ5GdFlth1vwObeVpTRM`
      const res = await fetch(api, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'grant_type=refresh_token&refresh_token=' + token,
      })
      if (!res.ok) throw new Error('ответ от сервера не ок')
      const data = await res.json()
      return {
        token: data.access_token,
        refreshToken: data.refresh_token,
      }
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async deleteChat(ref, data) {
    try {
      await firebase.database().ref(ref).set(data)
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async pushMessage(ref, data) {
    try {
      await firebase
        .database()
        .ref(ref)
        .push({ ...data, timestamp: firebase.database.ServerValue.TIMESTAMP })
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async updateChat(ref, childRef, data) {
    try {
      await firebase.database().ref(ref).child(childRef).update(data)
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async getChats(ref) {
    try {
      const res = await firebase.database().ref(ref).once('value')
      return await res.val()
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async removeItemChat(ref, childRef) {
    try {
      await firebase.database().ref(ref).child(childRef).remove()
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
  async searchChatsInDatabase(ref, typeSearch, search, status) {
    try {
      const searchArray = []
      await firebase
        .database()
        .ref(ref)
        .orderByChild('status')
        .equalTo(status)
        .once('value', (snapshot) => {
          snapshot.forEach((item) => {
            if (typeSearch === 'nickname') {
              if (
                item.val().client.toLowerCase().includes(search.toLowerCase())
              ) {
                searchArray.push({ key: item.key, content: item.val() })
              }
            }
            if (typeSearch === 'text') {
              if (
                item
                  .val()
                  .messages[0].content.toLowerCase()
                  .includes(search.toLowerCase())
              ) {
                searchArray.push({ key: item.key, content: item.val() })
              }
            }
          })
        })
      return searchArray
    } catch (e) {
      console.error(e)
      throw new Error(e)
    }
  }
}
export default new FirebaseApi()

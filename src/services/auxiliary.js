import moment from 'moment'
import 'moment/locale/ru'

moment().locale('ru')

const getTimeHuman = (timestamp) => {
  return moment(timestamp).fromNow()
}

const deleteItemInArrayMessages = (array, client) => {
  const newArray = [...array]
  const item = newArray.splice(
    newArray.findIndex((item) => item.content.client === client),
    1
  )
  const content = newArray.map((item) => item.content)
  return {
    content,
    item,
  }
}
const deleteItemInArrayChats = (array, key) => {
  const newArray = [...array]
  const item = newArray.splice(
    newArray.findIndex((item) => item.key === key),
    1
  )
  return {
    newArray,
    item,
  }
}
const replaceSymbols = (str) => {
  return str.replace(/[^a-zа-яё]/gi, '')
}
const limitationStringEllipsis = (str, length) => {
  if (str?.length >= length) return `${str.substring(0, length)}...`
  return str
}

const syncSearchChats = (arr, type, str) => {
  if (str === '') return arr
  if (type === 'nickname')
    return arr.filter((item) =>
      item.content.client.toLowerCase().includes(str.toLowerCase())
    )
  if (type === 'text')
    return arr.filter((item) =>
      item.content.messages[0]?.content
        ?.toLowerCase()
        .includes(str.toLowerCase())
    )
}

export {
  getTimeHuman,
  deleteItemInArrayMessages,
  replaceSymbols,
  limitationStringEllipsis,
  deleteItemInArrayChats,
  syncSearchChats,
}

import FormInput from '../FormInput/FormInput'
import PropTypes from 'prop-types'
import s from './ChatSearch.module.css'

const ChatSearch = ({
  searchChangeHandler,
  typeChangeHandler,
  valueSearch,
  valueType,
}) => {
  return (
    <div className={s.search}>
      <FormInput
        type="search"
        onChange={searchChangeHandler}
        value={valueSearch}
        name="search"
        autocomplete="text"
        label="Поиск"
      />
      <FormInput
        type="select"
        label="По"
        onChange={typeChangeHandler}
        value={valueType}
        name="select"
      >
        <option value="nickname">никнейму</option>
        <option value="text">тексту сообщения</option>
      </FormInput>
    </div>
  )
}
ChatSearch.propTypes = {
  searchChangeHandler: PropTypes.func.isRequired,
  typeChangeHandler: PropTypes.func.isRequired,
  valueType: PropTypes.string,
  valueSearch: PropTypes.string,
}
export default ChatSearch

import PropTypes from 'prop-types'
import s from './AppForm.module.css'
import { Form } from 'reactstrap'

const AppForm = ({ children, title, ...props }) => {
  return (
    <Form {...props} className={s.form}>
      <h2 className={s.title}>{title}</h2>
      {children}
    </Form>
  )
}
AppForm.propTypes = {
  title: PropTypes.string,
}
export default AppForm

import s from './ChatLayout.module.css'

const ChatLayout = ({ children }) => {
  return <div className={s.layout}>{children}</div>
}

export default ChatLayout

import ChatMenuItem from '../ChatMenuItem/ChatMenuItem'
import s from './ChatMenu.module.css'

const ChatMenu = () => {
  const clickHandler = (e) => {
    e.preventDefault()
  }
  return (
    <div className={s.menu}>
      <div className={s.topbar}>
        <span>
          <i className="far fa-comments" />
          N-CHAT
        </span>
        <a href=" " onClick={clickHandler}>
          <i className="fas fa-ellipsis-v" />
        </a>
      </div>
      <div className={s.navbar}>
        <ChatMenuItem link="/new" title="Новые" icon="fas fa-file" />
        <ChatMenuItem link="/active" title="Активные" icon="far fa-comment" />
        <ChatMenuItem link="/closed" title="Завершенные" icon="fas fa-times" />
        <ChatMenuItem link="/saved" title="Сохраненные" icon="far fa-save" />
      </div>
    </div>
  )
}

export default ChatMenu

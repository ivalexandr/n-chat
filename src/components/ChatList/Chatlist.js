import s from './ChatList.module.css'
const Chatlist = ({ children }) => {
  return <ul className={s.chatlist}>{children}</ul>
}

export default Chatlist

import cn from 'classnames'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import {
  asyncDeleteChatCreator,
  removeChatCreator,
  removeItemChatsCreator,
  setActiveCreator,
  updateChatCreator,
} from '../../redux/reducers/ChatReducer/ChatReducer'
import { newMessages } from '../../redux/reducers/ChatReducer/selectors'
import {
  deleteItemInArrayMessages,
  replaceSymbols,
} from '../../services/auxiliary'
import { email } from '../../redux/reducers/FormReducer/selectors'
import s from './ChatMessage.module.css'
import RatingStar from '../RatingStart/RatingStart'

const ChatMessage = ({
  avatar,
  clientName,
  messageText,
  textLink,
  dateStart,
  textSavedButton,
  dataClick,
  typeClickLink,
  iconClass,
  _id,
  rating,
}) => {
  const history = useHistory()
  const dispatch = useDispatch()
  const messages = useSelector(newMessages)
  const userEmail = useSelector(email)

  const clickOpenHandler = (e) => {
    e.preventDefault()
    if (e.target.dataset.click === 'open') {
      dispatch(
        asyncDeleteChatCreator({
          ref: 'new',
          data: deleteItemInArrayMessages(messages, clientName).content,
        })
      )
      dispatch(
        setActiveCreator({
          ref: replaceSymbols(userEmail),
          data: {
            ...deleteItemInArrayMessages(messages, clientName).item[0].content,
            status: 'active',
          },
        })
      )
      history.push(`/dialog/${clientName}`)
    }
    if (e.target.dataset.click === 'continue') {
      history.push(`/dialog/${clientName}`)
    }
    if (e.target.dataset.click === 'delete') {
      dispatch(removeItemChatsCreator(_id))
      dispatch(
        removeChatCreator({
          ref: replaceSymbols(userEmail),
          childRef: _id,
        })
      )
    }
  }
  const clickSavedButtonHandler = (e) => {
    e.preventDefault()
    if (e.target.dataset.click === 'close') {
      dispatch(removeItemChatsCreator(_id))
      dispatch(
        updateChatCreator({
          ref: replaceSymbols(userEmail),
          childRef: _id,
          data: { status: 'closed' },
        })
      )
    }
    if (e.target.dataset.click === 'save') {
      dispatch(removeItemChatsCreator(_id))
      dispatch(
        updateChatCreator({
          ref: replaceSymbols(userEmail),
          childRef: _id,
          data: { status: 'saved' },
        })
      )
    }
    if (e.target.dataset.click === 'delete') {
      dispatch(removeItemChatsCreator(_id))
      dispatch(
        removeChatCreator({
          ref: replaceSymbols(userEmail),
          childRef: _id,
        })
      )
    }
  }
  return (
    <li className={s.message}>
      <div className={cn(s.avatar, s.wrapper)}>
        <div className={s.image}>
          {avatar ? (
            <img src={avatar} alt="avatar" />
          ) : (
            <i className="fas fa-user-tie" />
          )}
        </div>
        {clientName}
      </div>
      <div className={cn(s.text, s.wrapper)}>{messageText}</div>
      <div className={cn(s.navigation, s.wrapper)}>
        {textLink && (
          <a
            href="/"
            className={s.link}
            onClick={clickOpenHandler}
            data-click={dataClick}
          >
            {textLink}
          </a>
        )}
        {textSavedButton && (
          <a
            className={s.savebutton}
            href="/"
            onClick={clickSavedButtonHandler}
            data-click={typeClickLink}
          >
            {textSavedButton} <i className={iconClass} />
          </a>
        )}
        {rating && <RatingStar rating={rating} />}
        <div className={s.date}>{dateStart}</div>
      </div>
    </li>
  )
}
ChatMessage.propTypes = {
  avatar: PropTypes.string,
  clientName: PropTypes.string,
  messageText: PropTypes.string,
  textLink: PropTypes.string,
  dateStart: PropTypes.string,
  textSavedButton: PropTypes.string,
  dataClick: PropTypes.string,
  iconClass: PropTypes.string,
  typeClickLink: PropTypes.string,
  _id: PropTypes.string,
  rating: PropTypes.number,
}
export default ChatMessage

import FormButton from '../FormButton/FormButton'
import PropTypes from 'prop-types'
import ChatMessagesBox from '../ChatMessagesBox/ChatMessagesBox'
import { useHistory } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import { logoutUserCreator } from '../../redux/reducers/FormReducer/FormReducer'
import { user } from '../../redux/reducers/FormReducer/selectors'
import s from './ChatMainWindow.module.css'

const ChatMainWindow = ({ children, className, queue }) => {
  const history = useHistory()
  const dispatch = useDispatch()
  const { email } = useSelector(user)
  const clickHandler = () => {
    dispatch(logoutUserCreator())
    history.push('/auth')
  }
  return (
    <div className={s.main}>
      <div className={s.topbar}>
        {queue && <span className={s.queue}>Клиентов в очереди: {queue}</span>}
        <h2 className={s.operator}>{email}</h2>
        <FormButton type="button" color="danger" onClick={clickHandler}>
          Выйти
        </FormButton>
      </div>
      <ChatMessagesBox className={className}>{children}</ChatMessagesBox>
    </div>
  )
}
ChatMainWindow.propTypes = {
  className: PropTypes.string,
  queue: PropTypes.string,
}
export default ChatMainWindow

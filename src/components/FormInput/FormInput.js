import PropTypes from 'prop-types'
import { Input } from 'reactstrap'
import s from './FormInput.module.css'

const FormInput = ({
  type = 'text',
  onChange,
  value,
  label,
  placeholder,
  name,
  autocomplete,
  ...props
}) => {
  return (
    <label className={s.label}>
      <span className={s.title}>{label}</span>
      <Input
        {...props}
        className={s.input}
        type={type}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
        name={name}
        autoComplete={autocomplete}
      />
    </label>
  )
}
FormInput.propTypes = {
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string.isRequired,
  autocomplete: PropTypes.string,
}
export default FormInput

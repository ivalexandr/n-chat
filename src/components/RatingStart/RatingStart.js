import PropTypes from 'prop-types'
import s from './RatingStar.module.css'

const RatingStar = ({ rating }) => {
  return (
    <div className={s.wrapper}>
      {[...Array(5)].map((star, index) => {
        const ratingValue = index + 1
        return (
          <label key={index}>
            <input type="radio" name="rating" value={ratingValue} />
            <i
              className={ratingValue <= rating ? 'fas fa-star' : 'far fa-star'}
            />
          </label>
        )
      })}
    </div>
  )
}
RatingStar.propTypes = {
  rating: PropTypes.number,
}
export default RatingStar

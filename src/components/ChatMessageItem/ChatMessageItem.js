import PropTypes from 'prop-types'
import cn from 'classnames'
import s from './ChatMessageItem.module.css'

const ChatMessageItem = ({ client, text, time }) => {
  return (
    <li className={cn(s.item, { [s.client]: client })}>
      {text}
      <time className={s.time}>{time}</time>
    </li>
  )
}
ChatMessageItem.propTypes = {
  client: PropTypes.bool,
  text: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
}
export default ChatMessageItem

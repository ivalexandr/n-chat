import s from './ChatBackground.module.css'

const ChatBackground = ({ children }) => {
  return <div className={s.background}>{children}</div>
}
export default ChatBackground

import { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { user } from '../../redux/reducers/FormReducer/selectors'
import { refreshUserCreator } from '../../redux/reducers/FormReducer/FormReducer'

import Main from '../../pages/Main/Main'
import ActiveMessages from '../../pages/ActiveMessages/ActiveMessages'
import NewMessages from '../../pages/NewMessages/NewMessages'
import SaveMessages from '../../pages/SaveMessages/SaveMessages'
import ClosedMessages from '../../pages/ClosedMessages/ClosedMessages'
import Dialog from '../../pages/Dialog/Dialog'
import Auth from '../../pages/Auth/Auth'
import Registration from '../../pages/Registration/Registration'
import Forgotten from '../../pages/Forgotten/Forgotten'
import ReloadPass from '../../pages/ReloadPass/ReloadPass'
import Page404 from '../../pages/404/404'

import ProtectedRoute from '../ProtectedRoute/ProtectedRoute'
import s from './App.module.css'

function App() {
  const token = useSelector(user)
  const dispatch = useDispatch()
  useEffect(() => {
    if (token) dispatch(refreshUserCreator(token?.refreshToken))
    // eslint-disable-next-line
  }, [])
  return (
    <Router>
      <div className={s.app}>
        <Switch>
          <ProtectedRoute exact path="/" children={<Main />} />
          <ProtectedRoute path="/active" children={<ActiveMessages />} />
          <ProtectedRoute path="/new" children={<NewMessages />} />
          <ProtectedRoute path="/saved" children={<SaveMessages />} />
          <ProtectedRoute path="/closed" children={<ClosedMessages />} />
          <ProtectedRoute path="/dialog/:id" children={<Dialog />} />
          <Route path="/auth" children={<Auth />} />
          <Route path="/reg" children={<Registration />} />
          <Route path="/forgotten" children={<Forgotten />} />
          <Route path="/reload" children={<ReloadPass />} />
          <Route path="*" children={<Page404 />} />
        </Switch>
      </div>
    </Router>
  )
}

export default App

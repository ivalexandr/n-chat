import PropTypes from 'prop-types'
import { Button } from 'reactstrap'

const FormButton = ({
  children,
  onClick,
  disabled,
  type,
  color = 'success',
  ...props
}) => {
  return (
    <Button
      {...props}
      onClick={onClick}
      disabled={disabled}
      type={type}
      color={color}
    >
      {children}
    </Button>
  )
}
FormButton.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  color: PropTypes.string,
}
export default FormButton

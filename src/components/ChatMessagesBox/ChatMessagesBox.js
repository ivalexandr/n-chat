import cn from 'classnames'
import PropTypes from 'prop-types'
import s from './ChatMessagesBox.module.css'

const ChatMessagesBox = ({ children, className }) => {
  return <div className={cn(s.box, className)}>{children}</div>
}

ChatMessagesBox.propTypes = {
  className: PropTypes.string,
}

export default ChatMessagesBox

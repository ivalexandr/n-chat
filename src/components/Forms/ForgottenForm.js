import { useEffect } from 'react'
import AppForm from '../AppForm/AppForm'
import FormInput from '../FormInput/FormInput'
import FormButton from '../FormButton/FormButton'
import { Link } from 'react-router-dom'
import cn from 'classnames'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { resetCreator } from '../../redux/reducers/FormReducer/FormReducer'
import { resetStatus } from '../../redux/reducers/FormReducer/selectors'
import { ToastContainer, toast } from 'react-toastify'
import s from './style.module.css'

const validate = (values) => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Обязательное поле'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email'
  }
  return errors
}

const ForgottenForm = () => {
  const dispatch = useDispatch()
  const status = useSelector(resetStatus)
  const success = () =>
    toast.success('Ссылка для сброса пароля отправлена!', {
      position: 'bottom-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    })

  useEffect(() => {
    if (status === 'success') success()
  }, [status])

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validate,
    onSubmit: (values) => {
      dispatch(resetCreator(values.email))
      success()
    },
    validateOnBlur: false,
    validateOnMount: false,
    validateOnChange: false,
  })
  return (
    <>
      <AppForm title="Восстановление пароля" onSubmit={formik.handleSubmit}>
        <div className={s.wrapper}>
          <FormInput
            onChange={formik.handleChange}
            value={formik.values.email}
            name="email"
            placeholder="Введите свой email..."
            autocomplete="email"
            label="Email"
          />
          {formik.errors.email && (
            <div className={s.error}>{formik.errors.email}</div>
          )}
        </div>
        <div className={cn(s.wrapper, s.button)}>
          <FormButton> Восстановить пароль </FormButton>
        </div>
        <div className={cn(s.wrapper, s.links, s.bottom)}>
          <Link to="/auth" className={s.link}>
            Войти
          </Link>
          <Link to="/reg" className={s.link}>
            Зарегистрироваться
          </Link>
        </div>
      </AppForm>
      <ToastContainer />
    </>
  )
}
export default ForgottenForm

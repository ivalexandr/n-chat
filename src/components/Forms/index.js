import AuthForm from './AuthForm'
import ForgottenForm from './ForgottenForm'
import RegForm from './RegForm'
import ReloadForm from './ReloadForm'

export { AuthForm, ForgottenForm, RegForm, ReloadForm }

import { useEffect } from 'react'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import FormInput from '../FormInput/FormInput'
import FormButton from '../FormButton/FormButton'
import AppForm from '../AppForm/AppForm'
import cn from 'classnames'
import { Link } from 'react-router-dom'
import { authStatus } from '../../redux/reducers/FormReducer/selectors'
import {
  authCreator,
  authUserWithGithubCreator,
  authUserWithGoogleCreator,
} from '../../redux/reducers/FormReducer/FormReducer'
import { ToastContainer, toast } from 'react-toastify'
import s from './style.module.css'

const validate = (values) => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Обязательное поле'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email'
  }
  if (!values.password) {
    errors.password = 'Обязательное поле'
  } else if (values.password.length <= 3) {
    errors.password = 'Пароль должен содержать больше 3 символов'
  }
  return errors
}

const AuthForm = () => {
  const dispatch = useDispatch()
  const status = useSelector(authStatus)
  const error = () =>
    toast.error('Не верный логин или пароль', {
      position: 'bottom-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    })
  const success = () =>
    toast.success('Вход выполнен успешно!', {
      position: 'bottom-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    })

  useEffect(() => {
    if (status === 'success') success()
    if (status === 'failed') error()
  }, [status])

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: (values) => {
      dispatch(authCreator(values))
    },
    validate,
    validateOnChange: false,
    validateOnBlur: false,
    validateOnMount: false,
  })
  const clickHandler = (e) => {
    e.preventDefault()
    if (e.target.dataset.click === 'Google') {
      dispatch(authUserWithGoogleCreator())
    }
    if (e.target.dataset.click === 'Github') {
      dispatch(authUserWithGithubCreator())
    }
  }
  return (
    <>
      <AppForm onSubmit={formik.handleSubmit} title="Авторизация">
        <div className={s.wrapper}>
          <FormInput
            label="Email"
            value={formik.values.email}
            onChange={formik.handleChange}
            placeholder="Введите свой email..."
            name="email"
            autocomplete="email"
          />
          {formik.errors.email && (
            <div className={s.error}>{formik.errors.email}</div>
          )}
        </div>
        <div className={s.wrapper}>
          <FormInput
            label="Пароль"
            value={formik.values.password}
            onChange={formik.handleChange}
            placeholder="Введите свой пароль..."
            type="password"
            name="password"
            autocomplete="current-password"
          />
          {formik.errors.password && (
            <div className={s.error}>{formik.errors.password}</div>
          )}
        </div>
        <div className={cn(s.wrapper, s.button)}>
          <FormButton type="submit"> Войти </FormButton>
        </div>
        <div className={cn(s.wrapper, s.socials)}>
          <a
            href="/"
            className={s.social}
            title="Войти через Github"
            onClick={clickHandler}
          >
            <i className="fab fa-github" data-click="Github" />
          </a>
          <a
            href="/"
            className={s.social}
            title="Войти через Google"
            onClick={clickHandler}
          >
            <i className="fab fa-google" data-click="Google" />
          </a>
        </div>
        <div className={cn(s.wrapper, s.links)}>
          <Link className={s.link} to="/reg">
            Зарегистрироваться
          </Link>
          <Link className={s.link} to="/forgotten">
            Забыли пароль?
          </Link>
        </div>
      </AppForm>
      <ToastContainer />
    </>
  )
}

export default AuthForm

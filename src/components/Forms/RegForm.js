import { useEffect } from 'react'
import AppForm from '../AppForm/AppForm'
import FormInput from '../FormInput/FormInput'
import FormButton from '../FormButton/FormButton'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { regStatus } from '../../redux/reducers/FormReducer/selectors'
import cn from 'classnames'
import { Link, useHistory } from 'react-router-dom'
import { regCreator } from '../../redux/reducers/FormReducer/FormReducer'
import { ToastContainer, toast } from 'react-toastify'
import s from './style.module.css'

const validate = (values) => {
  const errors = {}
  if (!values.email) {
    errors.email = 'Обязательное поле'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Некорректный email'
  }
  if (!values.password) {
    errors.password = 'Обязательное поле'
  } else if (
    !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(values.password)
  ) {
    errors.password =
      'Пароль должен содержать не менее 8 символов, должен содержать цифру и как минимум одну букву в верхнем регистре'
  } else if (values.password !== values.rePassword) {
    errors.rePassword = 'Пароли не совпадают'
  }
  return errors
}

const RegForm = () => {
  const history = useHistory()
  const status = useSelector(regStatus)

  const error = () =>
    toast.error('Возможно, пользователь с таким email уже существует', {
      position: 'bottom-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    })

  useEffect(() => {
    if (status === 'success') history.push('/')
    if (status === 'failed') error()
    // eslint-disable-next-line
  }, [status])

  const dispatch = useDispatch()
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      rePassword: '',
    },
    onSubmit(values) {
      dispatch(
        regCreator({
          email: values.email,
          password: values.password,
        })
      )
    },
    validate,
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
  })
  return (
    <>
      <AppForm title="Регистрация" onSubmit={formik.handleSubmit}>
        <div className={s.wrapper}>
          <FormInput
            label="Email"
            onChange={formik.handleChange}
            value={formik.values.email}
            name="email"
            autocomplete="email"
            placeholder="Введите свой email..."
            type="text"
          />
          {formik.errors.email && (
            <div className={s.error}>{formik.errors.email}</div>
          )}
        </div>
        <div className={s.wrapper}>
          <FormInput
            label="Пароль"
            onChange={formik.handleChange}
            value={formik.values.password}
            name="password"
            autocomplete="current-password"
            placeholder="Придумайте пароль..."
            type="password"
          />
          {formik.errors.password && (
            <div className={s.error}>{formik.errors.password}</div>
          )}
        </div>
        <div className={s.wrapper}>
          <FormInput
            label="Подтверждение пароля"
            onChange={formik.handleChange}
            value={formik.values.rePassword}
            name="rePassword"
            autocomplete="current-password"
            placeholder="Повторите пароль..."
            type="password"
          />
          {formik.errors.rePassword && (
            <div className={s.error}>{formik.errors.rePassword}</div>
          )}
        </div>
        <div className={cn(s.wrapper, s.button)}>
          <FormButton type="submit"> Зарегистрироваться </FormButton>
        </div>
        <div className={cn(s.wrapper, s.links)}>
          <Link className={s.link} to="/auth">
            Войти
          </Link>
          <Link className={s.link} to="/forgotten">
            Забыли пароль?
          </Link>
        </div>
      </AppForm>
      <ToastContainer />
    </>
  )
}
export default RegForm

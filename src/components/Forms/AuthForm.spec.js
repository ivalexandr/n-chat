import React from 'react'
import { shallow } from 'enzyme'
import AuthForm from './AuthForm'

describe('компонент авторизации отрисовался', () => {
  const form = shallow(
    <AuthForm submitHandler={() => console.log('submit!')} status="start" />
  )
  it('компонент содержит один заголовок типа h2', () => {
    expect(form.find('h2')).toHaveLength(1)
  })
  it('заголовок содержит слово Авторизация', () => {
    expect(form.find('h2').text()).toEqual('Авторизация')
  })
  it('компонент содержит два компонента FormInput', () => {
    expect(form.find('FormInput')).toHaveLength(2)
  })
  it('компонент содержит кнопку FormButton', () => {
    expect(form.find('FormButton')).toHaveLength(1)
  })
  it('кнопка содержит слово Войти', () => {
    expect(form.find('FormButton').render().text()).toEqual(' Войти ')
  })
})

import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import AppForm from '../AppForm/AppForm'
import FormInput from '../FormInput/FormInput'
import FormButton from '../FormButton/FormButton'
import { useFormik } from 'formik'
import cn from 'classnames'
import { Link } from 'react-router-dom'
import { useQuery } from '../../services/customHooks'
import { updateCreator } from '../../redux/reducers/FormReducer/FormReducer'
import { ToastContainer, toast } from 'react-toastify'
import { updateStatus } from '../../redux/reducers/FormReducer/selectors'
import { useHistory } from 'react-router'
import s from './style.module.css'

const validate = (values) => {
  const errors = {}
  if (!values.password) {
    errors.password = 'Обязательное поле'
  } else if (
    !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(values.password)
  ) {
    errors.password =
      'Пароль должен содержать не менее 8 символов, должен содержать цифру и как минимум одну букву в верхнем регистре'
  } else if (values.password !== values.rePassword) {
    errors.rePassword = 'Пароли не совпадают'
  }
  return errors
}

const ReloadForm = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const query = useQuery()
  const success = () =>
    toast.success('Пароль успешно изменен', {
      position: 'bottom-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    })

  const status = useSelector(updateStatus)

  useEffect(() => {
    if (status === 'success') {
      success()
      setTimeout(() => {
        history.push('/')
      }, 5000)
    }
    // eslint-disable-next-line
  }, [status])

  const formik = useFormik({
    initialValues: {
      password: '',
      rePassword: '',
    },
    onSubmit: (values) => {
      dispatch(
        updateCreator({
          token: query.get('oobCode'),
          password: values.password,
        })
      )
    },
    validate,
    validateOnChange: false,
    validateOnMount: false,
    validateOnBlur: false,
  })

  return (
    <>
      <AppForm title="Обновить пароль" onSubmit={formik.handleSubmit}>
        <div className={cn(s.wrapper)}>
          <FormInput
            onChange={formik.handleChange}
            value={formik.values.password}
            name="password"
            autocomplete="current-password"
            placeholder="Введите новый пароль..."
            label="Новый пароль"
            type="password"
          />
          {formik.errors.password && (
            <div className={s.error}>{formik.errors.password}</div>
          )}
        </div>
        <div className={cn(s.wrapper)}>
          <FormInput
            onChange={formik.handleChange}
            value={formik.values.rePassword}
            name="rePassword"
            autocomplete="current-password"
            placeholder="Повторите пароль..."
            label="Подтверждение пароля"
            type="password"
          />
          {formik.errors.rePassword && (
            <div className={s.error}>{formik.errors.rePassword}</div>
          )}
        </div>
        <div className={cn(s.wrapper, s.button)}>
          <FormButton type="submit">Изменить пароль</FormButton>
        </div>
        <div className={cn(s.wrapper, s.links)}>
          <Link className={s.link} to="/auth">
            Войти
          </Link>
          <Link className={s.link} to="/reg">
            Зарегистрироваться
          </Link>
        </div>
      </AppForm>
      <ToastContainer />
    </>
  )
}
export default ReloadForm

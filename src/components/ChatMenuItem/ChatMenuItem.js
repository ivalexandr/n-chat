import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import s from './ChatMenuItem.module.css'

const ChatMenuItem = ({ title, link, icon, clickHandler }) => {
  return (
    <NavLink
      to={link}
      className={s.menuitem}
      activeClassName={s.active}
      onClick={clickHandler}
    >
      <i className={icon} />
      {title}
    </NavLink>
  )
}

ChatMenuItem.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  icon: PropTypes.string,
  clickHandler: PropTypes.func,
}
export default ChatMenuItem

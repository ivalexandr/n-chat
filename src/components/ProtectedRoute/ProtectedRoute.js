import { Route, Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { user } from '../../redux/reducers/FormReducer/selectors'

const ProtectedRoute = ({ children, ...props }) => {
  const token = useSelector(user)

  return (
    <Route
      {...props}
      render={() => (token ? children : <Redirect to="/auth" />)}
    />
  )
}
export default ProtectedRoute

import firebase from 'firebase/app'
import {
  CLOSED_GET_NEW_MESSAGES,
  FAILED_GET_NEW_MESSAGES,
  START_GET_NEW_MESSAGES,
  SUCCESS_GET_NEW_MESSAGES,
  SUCCESS_GET_NEW_CHATS,
} from '../actions'

export const wsFirebase = () => {
  return (store) => {
    return (next) => (action) => {
      const { dispatch } = store
      const { type, payload } = action
      if (type === START_GET_NEW_MESSAGES) {
        const ref = firebase.database().ref(payload.ref)
        ref.on('value', (snapshot) => {
          if (snapshot.val()) {
            if (payload.type === 'chats') {
              dispatch({
                type: SUCCESS_GET_NEW_CHATS,
                payload: Object.entries(snapshot.val()).map((item) => ({
                  key: item[0],
                  content: item[1],
                })),
              })
            }
            if (payload.type === 'messages') {
              dispatch({
                type: SUCCESS_GET_NEW_MESSAGES,
                payload: Object.entries(snapshot.val()).find(
                  (item) => item[1]?.client === payload.id
                ),
              })
            }
          } else {
            dispatch({ type: FAILED_GET_NEW_MESSAGES })
          }
        })
      }
      if (type === CLOSED_GET_NEW_MESSAGES) {
        firebase.database().ref(payload).off()
      }
      next(action)
    }
  }
}

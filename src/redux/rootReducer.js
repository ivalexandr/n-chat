import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import ChatReducer from './reducers/ChatReducer/ChatReducer'
import FormReducer from './reducers/FormReducer/FormReducer'

const rootPersistConfig = {
  key: 'root',
  storage,
  stateReconciler: hardSet,
  whitelist: ['AUTH'],
}

const rootReducer = combineReducers({
  AUTH: FormReducer,
  CHAT: ChatReducer,
})
const persistedReducer = persistReducer(rootPersistConfig, rootReducer)

export default persistedReducer

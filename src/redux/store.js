import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'
import persistedReducer from './rootReducer'
import rootSaga from './sagas'
import { wsFirebase } from './middlewares/wsFirebase'

const sagaMiddleware = createSagaMiddleware()
const composeEnhancers =
  (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose
const enhancer = composeEnhancers(
  applyMiddleware(logger, sagaMiddleware, wsFirebase())
)
const store = () => {
  const store = createStore(persistedReducer, enhancer)
  const persistor = persistStore(store)
  sagaMiddleware.run(rootSaga)
  return {
    store,
    persistor,
  }
}

export default store

import {
  FAILED_AUTH_GITHUB_USER,
  FAILED_AUTH_GOOGLE_USER,
  FAILED_AUTH_USER,
  FAILED_LOGOUT_USER,
  FAILED_REFRESH_USER,
  FAILED_REG_USER,
  FAILED_RESET_PASSWORD,
  FAILED_UPDATE_PASSWORD,
  START_AUTH_GITHUB_USER,
  START_AUTH_GOOGLE_USER,
  START_AUTH_USER,
  START_LOGOUT_USER,
  START_REFRESH_USER,
  START_REG_USER,
  START_RESET_PASSWORD,
  START_UPDATE_PASSWORD,
  SUCCESS_AUTH_GITHUB_USER,
  SUCCESS_AUTH_GOOGLE_USER,
  SUCCESS_AUTH_USER,
  SUCCESS_LOGOUT_USER,
  SUCCESS_REFRESH_USER,
  SUCCESS_REG_USER,
  SUCCESS_RESET_PASSWORD,
  SUCCESS_UPDATE_PASSWORD,
} from '../../actions'

const initialState = {
  authStatus: '',
  regStatus: '',
  resetStatus: '',
  updateStatus: '',
  logoutStatus: '',
  refreshStatus: '',
  user: null,
}
const FormReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_AUTH_USER:
      return {
        ...state,
        authStatus: 'start',
      }
    case SUCCESS_AUTH_USER:
      return {
        ...state,
        authStatus: 'success',
        user: action.payload,
      }
    case FAILED_AUTH_USER:
      return {
        ...state,
        authStatus: 'failed',
      }
    case START_REG_USER:
      return {
        ...state,
        regStatus: 'start',
      }
    case SUCCESS_REG_USER:
      return {
        ...state,
        regStatus: 'success',
        user: action.payload,
      }
    case FAILED_REG_USER:
      return {
        ...state,
        regStatus: 'failed',
      }
    case START_RESET_PASSWORD:
      return {
        ...state,
        resetStatus: 'start',
      }
    case SUCCESS_RESET_PASSWORD:
      return {
        ...state,
        resetStatus: 'success',
      }
    case FAILED_RESET_PASSWORD:
      return {
        ...state,
        resetStatus: 'failed',
      }
    case START_UPDATE_PASSWORD:
      return {
        ...state,
        updateStatus: 'start',
      }
    case SUCCESS_UPDATE_PASSWORD:
      return {
        ...state,
        updateStatus: 'success',
      }
    case FAILED_UPDATE_PASSWORD:
      return {
        ...state,
        updateStatus: 'failed',
      }
    case START_AUTH_GOOGLE_USER:
      return {
        ...state,
        authStatus: 'start',
      }
    case SUCCESS_AUTH_GOOGLE_USER:
      return {
        ...state,
        authStatus: 'success',
        user: action.payload,
      }
    case FAILED_AUTH_GOOGLE_USER:
      return {
        ...state,
        authStatus: 'failed',
      }
    case START_AUTH_GITHUB_USER:
      return {
        ...state,
        authStatus: 'start',
      }
    case SUCCESS_AUTH_GITHUB_USER:
      return {
        ...state,
        authStatus: 'success',
        user: action.payload,
      }
    case FAILED_AUTH_GITHUB_USER:
      return {
        ...state,
        authStatus: 'failed',
      }
    case START_LOGOUT_USER:
      return {
        ...state,
        logoutStatus: 'start',
      }
    case SUCCESS_LOGOUT_USER:
      return {
        ...state,
        ...initialState,
        logoutStatus: 'sucess',
      }
    case FAILED_LOGOUT_USER:
      return {
        ...state,
        logoutStatus: 'failed',
      }
    case START_REFRESH_USER:
      return {
        ...state,
        refreshStatus: 'start',
      }
    case SUCCESS_REFRESH_USER:
      return {
        ...state,
        refreshStatus: 'success',
        user: {
          ...state.user,
          ...action.payload,
        },
      }
    case FAILED_REFRESH_USER:
      return {
        ...state,
        refreshStatus: 'failed',
      }
    default:
      return {
        ...state,
      }
  }
}
export const authCreator = (payload) => ({
  type: START_AUTH_USER,
  payload,
})
export const regCreator = (payload) => ({
  type: START_REG_USER,
  payload,
})
export const resetCreator = (payload) => ({
  type: START_RESET_PASSWORD,
  payload,
})
export const updateCreator = (payload) => ({
  type: START_UPDATE_PASSWORD,
  payload,
})
export const authUserWithGoogleCreator = (payload) => ({
  type: START_AUTH_GOOGLE_USER,
  payload,
})
export const authUserWithGithubCreator = (payload) => ({
  type: START_AUTH_GITHUB_USER,
  payload,
})
export const logoutUserCreator = (payload) => ({
  type: START_LOGOUT_USER,
  payload,
})
export const refreshUserCreator = (payload) => ({
  type: START_REFRESH_USER,
  payload,
})
export default FormReducer

export const authStatus = (store) => store.AUTH.authStatus
export const regStatus = (store) => store.AUTH.regStatus
export const resetStatus = (store) => store.AUTH.resetStatus
export const updateStatus = (store) => store.AUTH.updateStatus
export const user = (store) => store.AUTH.user
export const email = (store) => store.AUTH.user.email

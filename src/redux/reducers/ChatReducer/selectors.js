export const newMessages = (store) => store.CHAT?.newChats
export const messages = (store) => store.CHAT?.messages
export const targetMessage = (store) => store.CHAT?.targetMessage
export const status = (store) => store.CHAT?.status
export const statusActive = (store) => store.CHAT?.statusActive
export const chats = (store) => store.CHAT?.chats
export const statusChats = (store) => store.CHAT?.statusChats
export const searchChat = (store) => store.CHAT?.searchChat

import { deleteItemInArrayChats } from '../../../services/auxiliary'
import {
  FAILED_GET_NEW_MESSAGES,
  START_GET_NEW_MESSAGES,
  SUCCESS_GET_NEW_MESSAGES,
  SUCCESS_GET_NEW_CHATS,
  ASYNC_DELETE_CHAT_NEW_MESSAGES,
  CLOSED_GET_NEW_MESSAGES,
  START_SET_ACTIVE_MESSAGES,
  SUCCESS_SET_ACTIVE_MESSAGES,
  FAILED_SET_ACTIVE_MESSAGES,
  START_GET_CHATS,
  SUCCESS_GET_CHATS,
  FAILED_GET_CHATS,
  REMOVE_ITEM_CHATS,
  START_UPDATE_CHAT,
  SUCCESS_UPDATE_CHAT,
  FAILED_UPDATE_CHAT,
  START_REMOVE_CHAT,
  SYNC_SEARCH_CHATS,
  START_ASYNC_SEARCH_CHATS,
  SUCCESS_ASYNC_SEARCH_CHATS,
  FAILED_ASYNC_SEARCH_CHATS,
} from '../../actions'

const initialState = {
  newChats: null,
  searchChat: null,
  messages: null,
  targetMessage: null,
  chats: null,
  statusChats: '',
  statusActive: '',
  statusUpdate: '',
  status: '',
  searchStatus: '',
}

const ChatReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS_GET_NEW_CHATS:
      return {
        ...state,
        status: 'success',
        newChats: action.payload,
      }
    case SUCCESS_GET_NEW_MESSAGES:
      return {
        ...state,
        messages:
          action.payload && Object.entries(action.payload?.[1]?.messages),
        targetMessage: action.payload,
      }
    case FAILED_GET_NEW_MESSAGES:
      return {
        ...state,
        status: 'failed',
        newChats: null,
        messages: null,
        targetMessage: null,
      }
    case START_SET_ACTIVE_MESSAGES:
      return {
        ...state,
        statusActive: 'start',
      }
    case SUCCESS_SET_ACTIVE_MESSAGES:
      return {
        ...state,
        statusActive: 'success',
      }
    case FAILED_SET_ACTIVE_MESSAGES:
      return {
        ...state,
        statusActive: 'failed',
      }
    case START_GET_CHATS:
      return {
        ...state,
        statusChats: 'start',
      }
    case SUCCESS_GET_CHATS:
      return {
        ...state,
        statusChats: 'success',
        chats: Object.entries(action.payload.data)
          .map((item) => {
            if (item[1].status === action.payload.status) {
              return {
                key: item[0],
                content: item[1],
              }
            }
            return null
          })
          .filter((item) => item),
      }
    case FAILED_GET_CHATS:
      return {
        ...state,
        statusChats: 'failed',
      }
    case REMOVE_ITEM_CHATS: {
      return {
        ...state,
        chats: deleteItemInArrayChats(state.chats, action.payload).newArray,
      }
    }
    case START_UPDATE_CHAT:
      return {
        ...state,
        statusUpdate: 'start',
      }
    case SUCCESS_UPDATE_CHAT:
      return {
        ...state,
        statusUpdate: 'success',
      }
    case FAILED_UPDATE_CHAT:
      return {
        ...state,
        statusUpdate: 'failed',
      }
    case SYNC_SEARCH_CHATS:
      return {
        ...state,
        searchChat: action.payload,
      }
    case START_ASYNC_SEARCH_CHATS:
      return {
        ...state,
        searchStatus: 'start',
      }
    case SUCCESS_ASYNC_SEARCH_CHATS:
      return {
        ...state,
        searchStatus: 'success',
        searchChat: action.payload,
      }
    case FAILED_ASYNC_SEARCH_CHATS:
      return {
        ...state,
        searchStatus: 'failed',
      }
    default:
      return {
        ...state,
      }
  }
}
const newMessageCreator = (payload) => ({
  type: START_GET_NEW_MESSAGES,
  payload,
})
const asyncDeleteChatCreator = (payload) => ({
  type: ASYNC_DELETE_CHAT_NEW_MESSAGES,
  payload,
})
const closedGetNewMessagesCreator = (payload) => ({
  type: CLOSED_GET_NEW_MESSAGES,
  payload,
})
const setActiveCreator = (payload) => ({
  type: START_SET_ACTIVE_MESSAGES,
  payload,
})
const getChatsCreator = (payload) => ({
  type: START_GET_CHATS,
  payload,
})
const removeItemChatsCreator = (payload) => ({
  type: REMOVE_ITEM_CHATS,
  payload,
})
const updateChatCreator = (payload) => ({
  type: START_UPDATE_CHAT,
  payload,
})
const removeChatCreator = (payload) => ({
  type: START_REMOVE_CHAT,
  payload,
})
const syncSearchChatCreator = (payload) => ({
  type: SYNC_SEARCH_CHATS,
  payload,
})
const asyncSearchChatsCreator = (payload) => ({
  type: START_ASYNC_SEARCH_CHATS,
  payload,
})

export default ChatReducer

export {
  newMessageCreator,
  asyncDeleteChatCreator,
  closedGetNewMessagesCreator,
  setActiveCreator,
  getChatsCreator,
  removeItemChatsCreator,
  updateChatCreator,
  removeChatCreator,
  syncSearchChatCreator,
  asyncSearchChatsCreator,
}

import { all } from 'redux-saga/effects'
import { authUserWatcher } from './formSagas/authUserSaga'
import { regUserWatcher } from './formSagas/regUserSaga'
import { resetPasswordWatcher } from './formSagas/resetPasswordSaga'
import { updatePasswordWatcher } from './formSagas/updatePasswordSaga'
import { authUserWithGoogleWatcher } from './formSagas/authUserWithGoogleSaga'
import { authUserWithGithubWatcher } from './formSagas/authUserWithGithubSaga'
import { logoutUserWatcher } from './formSagas/logoutUserSaga'
import { refreshUserWatcher } from './formSagas/refreshUserSaga'
import { deleteChatSagaWatcher } from './chatSagas/deleteChatSaga'
import { pushMessageChatWatcher } from './chatSagas/pushMessage'
import { getChatsWatcher } from './chatSagas/getChatsSaga'
import { updateChatWatcher } from './chatSagas/updateChatSaga'
import { removeChatWatcher } from './chatSagas/removeChatSaga'
import { searchChatsWatcher } from './chatSagas/searchChatsSaga'

export default function* rootSaga() {
  yield all([
    authUserWatcher(),
    regUserWatcher(),
    resetPasswordWatcher(),
    updatePasswordWatcher(),
    authUserWithGoogleWatcher(),
    authUserWithGithubWatcher(),
    logoutUserWatcher(),
    refreshUserWatcher(),
    deleteChatSagaWatcher(),
    pushMessageChatWatcher(),
    getChatsWatcher(),
    updateChatWatcher(),
    removeChatWatcher(),
    searchChatsWatcher(),
  ])
}

import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../.././services/firebase'
import {
  FAILED_UPDATE_PASSWORD,
  START_UPDATE_PASSWORD,
  SUCCESS_UPDATE_PASSWORD,
} from '../../actions'

function* updatePasswordWorker(action) {
  try {
    const res = yield call(firebase.updatePassword, action.payload)
    yield put({ type: SUCCESS_UPDATE_PASSWORD, payload: res })
  } catch (e) {
    yield put({ type: FAILED_UPDATE_PASSWORD })
  }
}
export function* updatePasswordWatcher() {
  yield takeLatest(START_UPDATE_PASSWORD, updatePasswordWorker)
}

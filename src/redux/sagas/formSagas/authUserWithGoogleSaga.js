import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../.././services/firebase'
import {
  FAILED_AUTH_GOOGLE_USER,
  START_AUTH_GOOGLE_USER,
  SUCCESS_AUTH_GOOGLE_USER,
} from '../../actions'

function* authUserWithGoogleWorker() {
  try {
    const user = yield call(firebase.authWithGoogle)
    yield put({ type: SUCCESS_AUTH_GOOGLE_USER, payload: user })
  } catch (e) {
    yield put({ type: FAILED_AUTH_GOOGLE_USER })
  }
}

export function* authUserWithGoogleWatcher() {
  yield takeLatest(START_AUTH_GOOGLE_USER, authUserWithGoogleWorker)
}

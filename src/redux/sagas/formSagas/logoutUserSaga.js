import firebase from '../../.././services/firebase'
import { put, call, takeLatest } from 'redux-saga/effects'
import {
  FAILED_LOGOUT_USER,
  START_LOGOUT_USER,
  SUCCESS_LOGOUT_USER,
} from '../../actions'

function* logoutUserWorker() {
  try {
    yield call(firebase.logoutUser)
    yield put({ type: SUCCESS_LOGOUT_USER })
  } catch (e) {
    yield put({ type: FAILED_LOGOUT_USER })
  }
}
export function* logoutUserWatcher() {
  yield takeLatest(START_LOGOUT_USER, logoutUserWorker)
}

import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../.././services/firebase'
import {
  FAILED_RESET_PASSWORD,
  START_RESET_PASSWORD,
  SUCCESS_RESET_PASSWORD,
} from '../../actions'

function* resetPasswordWorker(action) {
  try {
    const res = yield call(firebase.resetPassword, action.payload)
    yield put({ type: SUCCESS_RESET_PASSWORD, payload: res })
  } catch (e) {
    yield put({ type: FAILED_RESET_PASSWORD })
  }
}
export function* resetPasswordWatcher() {
  yield takeLatest(START_RESET_PASSWORD, resetPasswordWorker)
}

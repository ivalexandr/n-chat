import firebase from '../../.././services/firebase'
import { put, takeLatest, call } from 'redux-saga/effects'
import {
  FAILED_REFRESH_USER,
  START_REFRESH_USER,
  SUCCESS_REFRESH_USER,
} from '../../actions'

function* refreshUserWorker(action) {
  try {
    const user = yield call(firebase.refreshUser, action.payload)
    yield put({ type: SUCCESS_REFRESH_USER, payload: user })
  } catch (e) {
    yield put({ type: FAILED_REFRESH_USER })
  }
}

export function* refreshUserWatcher() {
  yield takeLatest(START_REFRESH_USER, refreshUserWorker)
}

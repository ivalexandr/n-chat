import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../.././services/firebase'
import {
  START_REG_USER,
  SUCCESS_REG_USER,
  FAILED_REG_USER,
} from '../../actions'

function* regUserWorker(action) {
  try {
    const user = yield call(firebase.regUser, action.payload)
    yield put({ type: SUCCESS_REG_USER, payload: user })
  } catch (e) {
    yield put({ type: FAILED_REG_USER })
  }
}

export function* regUserWatcher() {
  yield takeLatest(START_REG_USER, regUserWorker)
}

import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../.././services/firebase'
import {
  FAILED_AUTH_USER,
  START_AUTH_USER,
  SUCCESS_AUTH_USER,
} from '../../actions'

function* authUserWorker(action) {
  try {
    const user = yield call(firebase.authUser, action.payload)
    yield put({ type: SUCCESS_AUTH_USER, payload: user })
  } catch (e) {
    yield put({ type: FAILED_AUTH_USER })
  }
}
export function* authUserWatcher() {
  yield takeLatest(START_AUTH_USER, authUserWorker)
}

import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../.././services/firebase'
import {
  FAILED_AUTH_GITHUB_USER,
  START_AUTH_GITHUB_USER,
  SUCCESS_AUTH_GITHUB_USER,
} from '../../actions'

function* authUserWithGithubWorker() {
  try {
    const user = yield call(firebase.authWithGithub)
    yield put({ type: SUCCESS_AUTH_GITHUB_USER, payload: user })
  } catch (e) {
    yield put({ type: FAILED_AUTH_GITHUB_USER })
  }
}
export function* authUserWithGithubWatcher() {
  yield takeLatest(START_AUTH_GITHUB_USER, authUserWithGithubWorker)
}

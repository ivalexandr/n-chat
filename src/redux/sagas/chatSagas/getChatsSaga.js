import { put, takeLatest, call } from 'redux-saga/effects'
import {
  FAILED_GET_CHATS,
  START_GET_CHATS,
  SUCCESS_GET_CHATS,
} from '../../actions'
import firebase from '../../../services/firebase'

function* getChatsWorker(action) {
  try {
    const data = yield call(firebase.getChats, action.payload.ref)
    yield put({
      type: SUCCESS_GET_CHATS,
      payload: { data, status: action.payload.status },
    })
  } catch (e) {
    yield put({ type: FAILED_GET_CHATS })
  }
}

export function* getChatsWatcher() {
  yield takeLatest(START_GET_CHATS, getChatsWorker)
}

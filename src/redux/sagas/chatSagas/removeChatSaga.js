import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../../services/firebase'
import {
  FAILED_REMOVE_CHAT,
  START_REMOVE_CHAT,
  SUCCESS_REMOVE_CHAT,
} from '../../actions'

function* removeChatWorker(action) {
  try {
    yield call(
      firebase.removeItemChat,
      action.payload.ref,
      action.payload.childRef
    )
    yield put({ type: SUCCESS_REMOVE_CHAT })
  } catch (e) {
    yield put({ type: FAILED_REMOVE_CHAT })
  }
}

export function* removeChatWatcher() {
  yield takeLatest(START_REMOVE_CHAT, removeChatWorker)
}

import { put, call, takeLatest } from 'redux-saga/effects'
import firebase from '../../../services/firebase'
import {
  FAILED_ASYNC_SEARCH_CHATS,
  START_ASYNC_SEARCH_CHATS,
  SUCCESS_ASYNC_SEARCH_CHATS,
} from '../../actions'

function* searchChatsWorker(action) {
  try {
    const data = yield call(
      firebase.searchChatsInDatabase,
      action.payload.ref,
      action.payload.typeSearch,
      action.payload.search,
      action.payload.status
    )
    yield put({ type: SUCCESS_ASYNC_SEARCH_CHATS, payload: data })
  } catch (e) {
    yield put({ type: FAILED_ASYNC_SEARCH_CHATS })
  }
}

export function* searchChatsWatcher() {
  yield takeLatest(START_ASYNC_SEARCH_CHATS, searchChatsWorker)
}

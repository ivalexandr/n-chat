import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../../services/firebase'
import {
  FAILED_SET_ACTIVE_MESSAGES,
  START_SET_ACTIVE_MESSAGES,
  SUCCESS_SET_ACTIVE_MESSAGES,
} from '../../actions'

function* pushMessageChatWorker(action) {
  try {
    yield call(firebase.pushMessage, action.payload.ref, action.payload.data)
    yield put({ type: SUCCESS_SET_ACTIVE_MESSAGES })
  } catch (e) {
    yield put({ type: FAILED_SET_ACTIVE_MESSAGES })
  }
}

export function* pushMessageChatWatcher() {
  yield takeLatest(START_SET_ACTIVE_MESSAGES, pushMessageChatWorker)
}

import { call, takeLatest } from 'redux-saga/effects'
import firebase from '../../../services/firebase'
import { ASYNC_DELETE_CHAT_NEW_MESSAGES } from '../../actions'

function* deleteChatSagaWorker(action) {
  yield call(firebase.deleteChat, action.payload.ref, action.payload.data)
}

export function* deleteChatSagaWatcher() {
  yield takeLatest(ASYNC_DELETE_CHAT_NEW_MESSAGES, deleteChatSagaWorker)
}

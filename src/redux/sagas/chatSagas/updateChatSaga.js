import { put, takeLatest, call } from 'redux-saga/effects'
import firebase from '../../../services/firebase'
import {
  FAILED_UPDATE_CHAT,
  START_UPDATE_CHAT,
  SUCCESS_UPDATE_CHAT,
} from '../../actions'

function* updateChatWorker(action) {
  try {
    yield call(
      firebase.updateChat,
      action.payload.ref,
      action.payload.childRef,
      action.payload.data
    )
    yield put({ type: SUCCESS_UPDATE_CHAT })
  } catch (e) {
    yield put({ type: FAILED_UPDATE_CHAT })
  }
}

export function* updateChatWatcher() {
  yield takeLatest(START_UPDATE_CHAT, updateChatWorker)
}

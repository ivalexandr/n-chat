# Проект N-CHAT.

## О проекте

Данный проект представляет собой чат службы поддержки для общения с клиентами, которые будут обращться в СП через мобильное приложение (будет разработано в дальнейшем). Стек технологий: React.js, redux, redux-saga. firebase.

## Основной функционал

- Возможность регистрации (по электронной почте, через аккаунт Google и через аккаунт Github), возможность авторизации. Также доступен функционал восстановления пароля по электронной почте.
- Возможность выбора категории (новые сообщения, текущие диалоги, завершенные диалоги, сохранненые диалоги).
- Возможность настройки своего профиля (имя пользователя, аватар, смена пароля через меня профиля)
- Возможность ведения диалогов (пока мобильное приложение не разработано, диалог вести не с кем).

## Основные пакеты, которые используются в сборке

Проект создан при помощи Create-react-app

- [Redux](https://www.npmjs.com/package/redux)
- [Redux-react](https://www.npmjs.com/package/react-redux)
- [Redux-saga](https://www.npmjs.com/package/redux-saga)
- [Redux-persist](https://www.npmjs.com/package/redux-persist)
- [React-router-dom](https://www.npmjs.com/package/react-router-dom "React-router-dom")
- [Redux-logger](https://www.npmjs.com/package/redux-logger "Redux-logger")
- [Classnames](https://www.npmjs.com/package/classnames "classnames")
- [Firebase](https://www.npmjs.com/package/firebase)
- [Reactstrap](https://www.npmjs.com/package/reactstrap) 
- [Moment.js](https://www.npmjs.com/package/moment)
- [React-toastify](https://www.npmjs.com/package/react-toastify)
- [Formik](https://www.npmjs.com/package/formik)
- [Final-form](npmjs.com/package/final-form)

## Инструкция по запуску
- Загрузить или клонировать данный репозиторий к себе;
- Выполнить установку пакетов  при помощи `npm i` или `yarn`;
- Запустить в development-режиме `npm run start` или `yarn start`;
- Запустить в production-режиме `npm run build` или `yarn build`;

Проект доступен по [ссылке](https://n-chat-2a591.web.app/)

Проект дорабатывается...
